#ifndef UNRN_CO2_Meter_h
#define UNRN_CO2_Meter_h

#include <Arduino.h>
#include <EEPROM.h>
#include <MHZ19_uart.h>
#include <Button_db.h>

#include <Wire.h>
#include <SSD1306.h>

// Necesarios para librería Wifi Manager
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

#include "images.h"

#define PRODUCTION_CODE

// Intervalo de precalentamiento
#ifdef PRODUCTION_CODE
    #define MHZ19_HEATING_TIME_SECONDS 3*60
#else
    #define MHZ19_HEATING_TIME_SECONDS 1
#endif
#define MHZ19_HEATING_TIME_MSECONDS MHZ19_HEATING_TIME_SECONDS*1000

// Intervalo entre mediciones de CO2
#define CO2_MEASURE_TIME_SECONDS 60
#define CO2_MEASURE_TIME_MSECONDS CO2_MEASURE_TIME_SECONDS*1000 

#define MHZ19_MIN_VAL 400
#define MHZ19_MAX_VAL 5000

// Intervalo entre reintentos de publicación de los datos
#define RETRY_IOT_TIME_SECONDS 5
#define RETRY_IOT_TIME_MSECONDS RETRY_IOT_TIME_SECONDS*1000 

// Tiempo de espera para confirmar la selección de un ítem del menú (calibración, o configuración del WiFi)
#define MENU_ITEM_TIMEOUT_SECONDS 30
#define MENU_ITEM_TIMEOUT_MSECONDS MENU_ITEM_TIMEOUT_SECONDS*1000

// Tiempo de espera en el estado de calibración
#ifdef PRODUCTION_CODE
    #define CALIBRATION_TIMEOUT_SECONDS 60*3
#else
    #define CALIBRATION_TIMEOUT_SECONDS 20
#endif
#define CALIBRATION_TIMEOUT_MSECONDS CALIBRATION_TIMEOUT_SECONDS*1000

// Tiempo de espera para configurar el wifi en el portal de configuración
#ifdef PRODUCTION_CODE
    #define PORTAL_TIMEOUT_SECONDS 60*3 //TODO: Verificar este valor. Medir cuánto tiempo me lleva
#else
    #define PORTAL_TIMEOUT_SECONDS 30 //TODO: Verificar este valor. Medir cuánto tiempo me lleva
#endif

#define WIFI_CONNECT_TIMEOUT_SECONDS 10

#define IOT_PARAMS_CONFIGURED    0xFFFFFFFF  // Todos los 32 bits tienen que estar seteados para indicar que los parámetros fueron configurados vía la página de configuración
#define IOT_PARAMS_NOT_CONFIGURED 0x0

// Estados luego de la pulsación del botón.
enum buttonPressedState{
        NONE,
        SHORT_PRESS,
        LONG_PRESS
    };

// Tiempos de pulsación corto y largo
const uint32_t buttonShortTime = 200;
const uint32_t buttonLongTime = 3000;

// Posibles estados del medidor de CO2
enum CO2_MeterState{
    HEATING,             // Precalientamiento de inicio
    MEASURING,           // Estado normal, midiendo y publicando los resultados
    CONFIRM_CALIBRATION, // Estado para confirmar la calibración
    CALIBRATION,         // Calibrando
    CONFIRM_WIFI_CONFIG, // Estado para confirmar la re-configuración del Wifi y las credenciales de Thingspeak
    WIFI_CONFIG,          // Portal de configuración on demand
    INFO
};

class UNRN_CO2_Meter
{    
    private:
        // Sensor de CO2, y pines asociados
        MHZ19_uart mhz19;
        const uint8_t MHZ19_RX = D5;
        const uint8_t MHZ19_TX = D8;
        
        // Partes por millón de CO2 leídas por el sensor
        int co2ppm;
        String co2_ppm_String;

        // Definiciones para los 3 LEDs
        const uint8_t greenLED = D7;
        const uint8_t yellowLED = D4;
        const uint8_t redLED = D3;

        bool greenLEDStatus = false;
        bool yellowLEDStatus = false;
        bool redLEDStatus = false;
        
        // Pulsador
        const uint8_t pushButton = D6;

        static SSD1306  display;
        
        WiFiClient client;
        HTTPClient http;

        // Wifi Manager
        WiFiManager wifiManager;
        String meterSsid;
        WiFiManagerParameter adafruitIO_user;
        WiFiManagerParameter adafruitIO_key;
        WiFiManagerParameter adafruitIO_feedkey;

        String meterIotServer = "http://io.adafruit.com/api/v2/";
        // Para guardar datos del servidor IoT (Thingspeak)
        struct {
            uint32_t configured;        // Todos los 32 bits tienen que estar seteados para indicar que los parámetros fueron configurados vía la página de configuración
            char adafruitIO_user[33];
            char adafruitIO_key[33];
            char adafruitIO_feedkey[128];
        }iotParams;

        bool iotError = false;

        char* Wifi_Icon_bits;

        // Variables para contar tiempos
        unsigned long lastTime, elapsedTime, lastIotErrorTime = 0;

        CO2_MeterState meterState;
        void mainFSM();

        // Pulsador con antirrebote. Debe llamarse al método fsmButtonUpdate() periódicamente.
        Button_db button;
        // Método para actualizar el estado del pulsador y medir la duración de las pulsaciones.
        void updateButton();
        buttonPressedState buttonState;
        buttonPressedState getButtonState();

        void ledON(uint8_t LED);
        void ledOFF(uint8_t LED);

        int getPPM();

        void updateDisplay();
        static void displayWifiInfo(String SSID);
        
        void print(String str);

        static void configModeCallback (WiFiManager *myWiFiManager);
        static void saveParamCallback();

        void adafruitIOPost();

    public:
        UNRN_CO2_Meter();
        ~UNRN_CO2_Meter();

        void begin();
        void runIteration();
};


#endif