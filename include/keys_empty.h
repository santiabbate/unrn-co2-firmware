#ifndef UNRN_CO2_KEYS_H
#define UNRN_CO2_KEYS_H

#include <Arduino.h>

// Wifi Network
const char WIFI_SSID[] = "Llenar con Nombre de la Red";
const char WIFI_PASS[] = "Llenar con contraseña";

// Thingspeak Key
String apiKey = "Llenar con la llave del canal de Thingspeak";
String server = "api.thingspeak.com";

#endif