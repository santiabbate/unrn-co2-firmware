#include "UNRN_CO2_Meter.h"

UNRN_CO2_Meter::UNRN_CO2_Meter(){}
UNRN_CO2_Meter::~UNRN_CO2_Meter(){}
SSD1306 UNRN_CO2_Meter::display(0x3C, D2, D1);

extern UNRN_CO2_Meter CO2_Meter;

/**
 * @brief Se llama cuando se guardan los parámetros de
 * Thingspeak en la web de configuración. Del formulario
 * de configuración se guardan el campo (field) dentro del canal
 * y la API Key de Thingspeak.
 */
void UNRN_CO2_Meter::saveParamCallback(){
    // Guardo los datos de Adafruit IO
    strncpy(CO2_Meter.iotParams.adafruitIO_user,CO2_Meter.adafruitIO_user.getValue(),33);
    strncpy(CO2_Meter.iotParams.adafruitIO_key,CO2_Meter.adafruitIO_key.getValue(),33);
    strncpy(CO2_Meter.iotParams.adafruitIO_feedkey,CO2_Meter.adafruitIO_feedkey.getValue(),128);

    #ifndef PRODUCTION_CODE
    Serial.println(CO2_Meter.iotParams.adafruitIO_user);
    Serial.println(CO2_Meter.iotParams.adafruitIO_key);
    Serial.println(CO2_Meter.iotParams.adafruitIO_feedkey);
    #endif

    CO2_Meter.iotParams.configured = IOT_PARAMS_CONFIGURED;

    // Copio los datos de forma persistente
    EEPROM.put(0,CO2_Meter.iotParams);
    EEPROM.commit();
}

/**
 * @brief Se ejecuta cuando el medidor no pudo conectarse a WiFi e
 * ingresa a modo Access Point. Esto sucede si tiene mal configurada la red
 * o cuando se inicia el portal de configuración on-demand desde el menú
 * @param myWiFiManager 
 */
void UNRN_CO2_Meter::configModeCallback (WiFiManager *myWiFiManager) {
    // Splashcreen: Información WiFi
    displayWifiInfo(myWiFiManager->getConfigPortalSSID());
}

/**
 * @brief Splashscreen en el display con información de conexión WiFi.
 * 
 * @param SSID Nombre de la red a conectarse.
 */
void UNRN_CO2_Meter::displayWifiInfo(String SSID) {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_10);
    display.drawStringMaxWidth(64, 0, 128, "Conectarse a red WiFi:");
    display.setFont(ArialMT_Plain_16);
    display.drawStringMaxWidth(64, 13, 128,SSID);
    display.setFont(ArialMT_Plain_10);
    display.drawStringMaxWidth(64, 31, 128,"Ingresar a la dirección:");
    display.setFont(ArialMT_Plain_16);
    display.drawStringMaxWidth(64, 46, 128,"192.168.4.1");
    display.display();
}

/**
 * @brief Inicialización del medidor
 * 
 */
void UNRN_CO2_Meter::begin()
{
    // Inicio sensor MHZ-19b
    mhz19.begin(this->MHZ19_RX, this->MHZ19_TX);
    mhz19.setAutoCalibration(false);

    // Inicializar GPIOs
    // LEDs
    pinMode(greenLED, OUTPUT);
    pinMode(yellowLED, OUTPUT);
    pinMode(redLED, OUTPUT);

    // Apago todos los LEDs
    ledOFF(greenLED);
    ledOFF(yellowLED);
    ledOFF(redLED);

    // Inicio el pulsador (máquina de estados antirrebote de la librería)
    button.begin(pushButton);

    // Display
    display.init();
    display.clear();
    display.flipScreenVertically();
    
    // Primer splashscreen: Logo UNRN
    display.drawXbm((128-Logo_width)/2, (64-Logo_height)/2, Logo_width, Logo_height, Logo_bits);
    display.display();
    delay(5000);
    // Segundo splashcreen: Presentación
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_16);
    display.drawStringMaxWidth(64, 12, 128, "Medidor CO2 UNRN");
    display.display();
    delay(2000);

    // Configuración de WiFi Manager
    
    // wifiManager.resetSettings();          // Descomentar para resetear todas las configuraciones guardadas
    WiFi.mode(WIFI_STA);        // Modo station - Inicia con el Access Point desactivado que se activa si no se pudo conectar a ninguna red
    #ifndef PRODUCTION_CODE
        Serial.setDebugOutput(true);
    #else
        Serial.setDebugOutput(false);
    #endif
    delay(3000);

    // Creo el menú del servidor web de configuración
    std::vector<const char *> menu = {"wifi","sep","erase","exit"};
    wifiManager.setMenu(menu);

    // Nombre de la red
    meterSsid = "CO2_" + String(WIFI_getChipId(),HEX);
    meterSsid.toUpperCase();

    // Configuro el callback que se llama cuando no puede conectarse a una red WiFi.
    // Cuando se llama al callback entra en modo AccessPoint
    wifiManager.setAPCallback(UNRN_CO2_Meter::configModeCallback);

    // Configuro parámetros extra del menú para los datos de adafruit io
    new (&this->adafruitIO_user) WiFiManagerParameter("adafruitIO_user", "1) Nombre de usuario/a de Adafruit IO", "", 40,"placeholder=\"Escribir aquí el nombre de usuario/a\"");
    wifiManager.addParameter(&this->adafruitIO_user);

    new (&this->adafruitIO_key) WiFiManagerParameter("adafruitIO_key", "<hr><br>2) Clave de la cuenta de Adafruit IO", "", 40,"placeholder=\"Escribir aquí la clave de la cuenta de Adafruit IO\"");
    wifiManager.addParameter(&this->adafruitIO_key);

    new (&this->adafruitIO_feedkey) WiFiManagerParameter("adafruitIO_feedkey", "<hr><br>3) Clave del feed de Adafruit IO para este medidor", "", 40,"placeholder=\"Escribir aquí la clave del feed de Adafruit IO\"");
    wifiManager.addParameter(&this->adafruitIO_feedkey);

    wifiManager.setSaveParamsCallback(saveParamCallback);
    
    // EEPROM para guardar permanentemente los parámetros de Adafruit IO
    EEPROM.begin(sizeof(iotParams));
    EEPROM.get(0,iotParams);
    if (iotParams.configured != IOT_PARAMS_CONFIGURED)
    {
        memset(&iotParams,0,sizeof(iotParams));
    }

    // wifiManager.setSaveConnectTimeout(10);
    wifiManager.setBreakAfterConfig(true);
    // wifiManager.setAPClientCheck(true); // avoid timeout if client connected to softap
    wifiManager.setConnectTimeout(WIFI_CONNECT_TIMEOUT_SECONDS); // How long to try to connect for before continuing
    wifiManager.setConfigPortalTimeout(PORTAL_TIMEOUT_SECONDS); // Auto close configportal after n seconds
    wifiManager.autoConnect(meterSsid.c_str());  // Conectar a última red configurada. Si falla crea un punto de acceso con meterSsid como nombre de red.

    // Estado inicial del medidor
    meterState = HEATING;
    lastTime = millis();
}

/**
 * @brief Actualiza el estado del medidor. Se ejecuta en el loop principal.
 * 
 */
void UNRN_CO2_Meter::runIteration()
{   
    elapsedTime = millis() - lastTime;

    // Leo estado del pulsador (se actualiza la FSM)
    updateButton();
    // Máquina de estados principal
    mainFSM();

    // ¿Estoy conectado a WiFi? => Actualizo ícono según corresponda
    if(WiFi.status() == WL_CONNECTED){
        Wifi_Icon_bits = Wifi_Connected_bits;
    }
    else{
        Wifi_Icon_bits = Wifi_Disconnected_bits;
    }
}


void UNRN_CO2_Meter::mainFSM(){
    static int lastco2ppm = 0;           
    
    buttonPressedState btn = getButtonState();
    
    switch (meterState)
    {
    case HEATING:
        static int heatingEntryFlag = 0;
        if (heatingEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            heatingEntryFlag = 1;
        }

        updateDisplay();
        if (elapsedTime >= MHZ19_HEATING_TIME_MSECONDS)
        {
            meterState = MEASURING;
            heatingEntryFlag = 0;
            lastTime = millis();
        }
        break;

    case MEASURING:
        static int measuringEntryFlag = 0;
        if (measuringEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            measuringEntryFlag++;
        }

        // ¿Ya es momento de medir CO2?
        if (elapsedTime >= CO2_MEASURE_TIME_MSECONDS || (measuringEntryFlag == 1))
        {
            measuringEntryFlag++;
            lastco2ppm = co2ppm;
            co2ppm = getPPM();

            #ifndef PRODUCTION_CODE
            Serial.println(co2ppm);
            #endif

            if ((co2ppm < MHZ19_MIN_VAL))
            {
                co2ppm = 0;
            }
            else if (co2ppm > MHZ19_MAX_VAL)
            {
                co2ppm = MHZ19_MAX_VAL;
            }
            
            co2_ppm_String = String(co2ppm);

            adafruitIOPost();

            updateDisplay();
            lastTime = millis();
        }

        // Si hubo error para publicar los datos y todavía no hay que medir, reintento subir la medición
        if (iotError && (elapsedTime <= CO2_MEASURE_TIME_MSECONDS))
        {
            if ((long)(elapsedTime - lastIotErrorTime) >= RETRY_IOT_TIME_MSECONDS)
            {
                adafruitIOPost();
                updateDisplay();
            }
        }
        
        // Condición de salida del estado
        if (btn == LONG_PRESS)
        {
            meterState = CONFIRM_WIFI_CONFIG;
            measuringEntryFlag = 0;
        }

        break;

    case CONFIRM_CALIBRATION:
        static int confirmCalibrationEntryFlag = 0;
        if (confirmCalibrationEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            confirmCalibrationEntryFlag = 1;
        }
        updateDisplay();
        if (btn == SHORT_PRESS)
        {
            meterState = MEASURING;
        }
        else if (btn == LONG_PRESS)
        {
            meterState = CALIBRATION;
        }
        btn = NONE;

        // Salida por timeout
        if (elapsedTime > MENU_ITEM_TIMEOUT_MSECONDS)
        {
            meterState = MEASURING;
        }

        // Acción de salida del estado
        if (meterState != CONFIRM_CALIBRATION){
            updateDisplay();
            confirmCalibrationEntryFlag = 0;
        }
        break;

    case CALIBRATION:
        
        static int calibrationEntryFlag = 0;
        if (calibrationEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            calibrationEntryFlag = 1;
        }

        // Salgo del estado luego del tiempo definido
        if (elapsedTime > CALIBRATION_TIMEOUT_MSECONDS){ 
            // Calibro el sensor al salir del estado: Me aseguro que pasó el tiempo de estabilización
            #ifdef PRODUCTION_CODE
            mhz19.calibrateZero();
            #endif
            meterState = MEASURING;
            calibrationEntryFlag = 0;
        }

        updateDisplay();
        break;

    case CONFIRM_WIFI_CONFIG:
        static int confirmWiFiConfigEntryFlag = 0;
        if (confirmWiFiConfigEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            confirmWiFiConfigEntryFlag = 1;
        }
        updateDisplay();
        if (btn == SHORT_PRESS)
        {
            meterState = INFO;
        }
        else if (btn == LONG_PRESS)
        {
            meterState = WIFI_CONFIG;
        }
        btn = NONE;

        // Salida por timeout
        if (elapsedTime > MENU_ITEM_TIMEOUT_MSECONDS)
        {
            meterState = MEASURING;
        }

        // Acción de salida del estado
        if (meterState != CONFIRM_WIFI_CONFIG){
            updateDisplay();
            confirmWiFiConfigEntryFlag = 0;
        }
        break;
    
    case WIFI_CONFIG:
        wifiManager.startConfigPortal(meterSsid.c_str());
        meterState = MEASURING;
    break;

    case INFO:
        static int infoEntryFlag = 0;
        if (infoEntryFlag == 0){
            // Empiezo a contar tiempo al entrar al estado
            lastTime = millis();
            elapsedTime = 0;
            infoEntryFlag = 1;
            updateDisplay();
        Serial.println(iotParams.adafruitIO_feedkey);
        }


        if (btn == SHORT_PRESS)
        {
            meterState = CONFIRM_CALIBRATION;
        }
        btn = NONE;

        // Salida por timeout
        if (elapsedTime > MENU_ITEM_TIMEOUT_MSECONDS)
        {
            meterState = MEASURING;
        }

        break;

    default:
        break;
    }
}

void UNRN_CO2_Meter::adafruitIOPost(){
    
    bool ret;
    
    // {username}/feeds/{feed_key}/data
    String url = meterIotServer + iotParams.adafruitIO_user + "/feeds/" + iotParams.adafruitIO_feedkey + "/data";
    ret = http.begin(client, url);

    #ifndef PRODUCTION_CODE
    Serial.println(url);
    Serial.println("HTTP Begin ret code:");
    Serial.println(ret);
    #endif

    http.addHeader("X-AIO-Key", iotParams.adafruitIO_key);
    http.addHeader("Content-Type", "application/json");
    
    String httpRequestData = "{ \"value\":\"" + co2_ppm_String + "\" }";
    int httpResponseCode = http.POST(httpRequestData);;

    #ifndef PRODUCTION_CODE
    Serial.println("HTTP Request Data:");
    Serial.println(httpRequestData);
    Serial.println("HTTP Response String:");
    Serial.println(http.getString());
    #endif
    
    if (httpResponseCode != 200)
    {
        iotError = true;
        lastIotErrorTime = millis();
        #ifndef PRODUCTION_CODE
        Serial.println("ERROR Adafruit. Response code:");
        Serial.println(httpResponseCode);
        #endif
    }
    else{
        iotError = false;
        #ifndef PRODUCTION_CODE
        Serial.println("Publicado");
        Serial.println("Response:");
        Serial.println(httpResponseCode);
        #endif
    }
    
    
    http.end();
}

void UNRN_CO2_Meter::ledON(uint8_t LED){
    digitalWrite(LED, LOW);
}

void UNRN_CO2_Meter::ledOFF(uint8_t LED){
    digitalWrite(LED, HIGH);
}

int UNRN_CO2_Meter::getPPM() {
	int ppm = mhz19.getPPM();
    return ppm;
}

void UNRN_CO2_Meter::updateDisplay() {
    
    uint8_t percent;

    String info;
    switch (meterState)
    {
    case HEATING:
        display.clear();
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.setFont(ArialMT_Plain_16);
        display.drawStringMaxWidth(64, 10, 128, "Precalentando");

        percent = (uint8_t) ((100*elapsedTime) / ((unsigned long) MHZ19_HEATING_TIME_MSECONDS));

        display.drawProgressBar(8,40,112,6,percent);

        display.display();
        break;

    case MEASURING:
        if (co2ppm >= 400 && co2ppm <= 600)
        {
            ledON(greenLED);
            ledOFF(yellowLED);
            ledOFF(redLED);
            info = "Buena ventilación";
        }
        else if (co2ppm > 600 && co2ppm <= 700)
        {
            ledOFF(greenLED);
            ledON(yellowLED);
            ledOFF(redLED);
            info = "Ventilación media";
        }
        else if (co2ppm > 700)
        {
            ledOFF(greenLED);
            ledOFF(yellowLED);
            ledON(redLED);
            info = "Ventilar";
        }

        display.clear();
        
        display.setFont(ArialMT_Plain_16);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(62,0,"CO2");
        display.setFont(ArialMT_Plain_24);
        display.drawString(64,20,String(co2ppm));
        display.setFont(ArialMT_Plain_10);

        // Me fijo si el valor de co2 tiene más de 3 dígitos y corro el texto de "ppm"
        if (co2ppm > 999)
        {
            display.drawString(105,32,"ppm");
        }
        else{
            display.drawString(96,32,"ppm");

        }
        
        display.drawString(64,48,info);
        
        display.drawXbm(0, 3, Wifi_Icon_height, Wifi_Icon_width, this->Wifi_Icon_bits);

        if (iotError)
        {
            display.setFont(ArialMT_Plain_10);
            display.drawString(115,0,"Error");
            display.drawString(115,10,"IoT");
        }
        
        break;
    
    case CONFIRM_CALIBRATION:
        display.clear();
        display.setFont(ArialMT_Plain_16);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(62,0,"CALIBRACIÓN");
        display.drawStringMaxWidth(64, 20, 128, "Pulsación larga para confirmar");
        break;

    case CALIBRATION:
        display.clear();
        display.setFont(ArialMT_Plain_16);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(62,0,"CALIBRANDO");
        display.setFont(ArialMT_Plain_10);
        display.drawStringMaxWidth(62,20,128,"Dejar el medidor al exterior hasta finalizar");
        
        percent = (uint8_t) ((100*elapsedTime) / ((unsigned long) CALIBRATION_TIMEOUT_MSECONDS));

        display.drawProgressBar(8,49,112,6,percent);
        
        break;

    case CONFIRM_WIFI_CONFIG:
        display.clear();
        display.setFont(ArialMT_Plain_16);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawStringMaxWidth(62,0,128,"CONFIGURAR WIFI");
        display.setFont(ArialMT_Plain_10);
        display.drawStringMaxWidth(64, 40, 128, "Pulsación larga para confirmar");
        break;

    case INFO:
        display.clear();
        display.setFont(ArialMT_Plain_10);
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawStringMaxWidth(62,0,128,"Configuración:");
        display.setTextAlignment(TEXT_ALIGN_LEFT);
        display.setFont(ArialMT_Plain_10);
        display.drawStringMaxWidth(0, 12, 128, "Red WIFI: " + wifiManager.getWiFiSSID());
        display.drawStringMaxWidth(0, 24, 128, "Usuario adafruit: " + String(iotParams.adafruitIO_user));
        display.drawStringMaxWidth(0, 36, 128, "Feed: ");
        display.drawStringMaxWidth(0, 48, 128,String(iotParams.adafruitIO_feedkey));
        break;

    default:
        break;
    }
    
    
    display.display();

    
}

void UNRN_CO2_Meter::updateButton(){

    static bool pressed = 0;
    static bool longPressFlag = false;
    static bool counting = false;
    static bool lastWasLongPress = false;
    static unsigned long initialTime = 0;
    unsigned long buttonPressedTime = 0;
    unsigned long actualTime;
    button.fsmButtonUpdate();

    // La primera vez que se presiona el botón, empiezo a contar tiempo para determinar el largo de la pulsación
    if((button.fsmButtonState == STATE_BUTTON_DOWN) & !pressed){
        pressed = 1;
        initialTime = millis();
        counting = true;
    }

    actualTime = millis();
    buttonPressedTime = actualTime - initialTime;

    if (counting)
    {
        longPressFlag = (buttonPressedTime > buttonLongTime) ? true : false;
    }
    else{
        longPressFlag = false;
    }
    
    if (pressed && longPressFlag && !lastWasLongPress)
    {
        buttonState = LONG_PRESS;
        lastWasLongPress = true;
        // pressed = 0;
        counting = false;

        #ifndef PRODUCTION_CODE
        if (buttonState == LONG_PRESS){
            Serial.println("LONG_PRESS");
        }
        else if (buttonState == SHORT_PRESS){
            Serial.println("SHORT_PRESS");
        }
        #endif

    }
    
    if (pressed && button.fsmButtonState == STATE_BUTTON_UP)
    {
        if (lastWasLongPress)
        {
            buttonState = NONE;
            lastWasLongPress = false;
        }
        else
        {
            buttonState = SHORT_PRESS;
        }
        pressed = 0;
        counting = false;
        
        #ifndef PRODUCTION_CODE
        if (buttonState == LONG_PRESS){
            Serial.println("LONG_PRESS");
        }
        else if (buttonState == SHORT_PRESS){
            Serial.println("SHORT_PRESS");
        }
        #endif
    } 
}

buttonPressedState UNRN_CO2_Meter::getButtonState(){
    // Almaceno estado del pulsador
    buttonPressedState s = buttonState;
    // Borro el estado porque fué "consumido" por alguien
    buttonState = NONE;
    return s;
}

void UNRN_CO2_Meter::print(String text){
    display.clear();
    display.drawStringMaxWidth(64, 12, 128, text);
    display.display();
}