#include <Arduino.h>

#include <MHZ19_uart.h>
#include <ESP8266WiFi.h>

#include "UNRN_CO2_Meter.h"

UNRN_CO2_Meter CO2_Meter;

void setup() {
  Serial.begin(115200);
  CO2_Meter.begin();
}

// Loop principal
void loop() {
  CO2_Meter.runIteration();
  delay(1);
}
