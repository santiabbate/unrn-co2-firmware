#include <Arduino.h>

typedef enum{
   STATE_BUTTON_UP,
   STATE_BUTTON_DOWN,
   STATE_BUTTON_FALLING,
   STATE_BUTTON_RISING
} fsmButtonState_t;

class Button_db
{
private:
    uint8_t pin;
public:
    Button_db(/* args */);
    ~Button_db();

    void begin(uint8_t pin);

    void fsmButtonUpdate();
    //void fsmButtonError( void );
    //void fsmButtonInit( void );
    //void buttonPressed( void );
    //void buttonReleased( void );
    fsmButtonState_t fsmButtonState;
};


