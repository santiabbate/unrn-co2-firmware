#include "Button_db.h"

Button_db::Button_db(/* args */)
{
}

Button_db::~Button_db()
{
}

void Button_db::begin(uint8_t pin){
    
    this->pin = pin;
    pinMode(pin,INPUT); // Mapeo botón al pin correspondiente

    fsmButtonState = STATE_BUTTON_UP; // Estado inicial. Botón sin presionar.
}


void Button_db::fsmButtonUpdate( void )
{
   static bool flagFalling = 0;
   static bool flagRising = 0;
   
   static uint32_t contFalling = 0;
   static uint32_t contRising = 0;

   switch( fsmButtonState ){

      case STATE_BUTTON_UP: 
         /* CHECK TRANSITION CONDITIONS */
         if( !digitalRead(pin) ){
            fsmButtonState = STATE_BUTTON_FALLING;
            contFalling = millis(); // Tiempo actual
         }
      break;

      case STATE_BUTTON_DOWN:
         /* CHECK TRANSITION CONDITIONS */
         if( digitalRead(pin) ){
            fsmButtonState = STATE_BUTTON_RISING;
            contRising = millis(); // Tiempo actual
         }
      break;

      case STATE_BUTTON_FALLING:      
         /* ENTRY */
         if( flagFalling == 0 ){
            flagFalling = 1;
         }      
         /* CHECK TRANSITION CONDITIONS */
         if( millis() - contFalling >= 40 ){ // Ya pasó el tiempo de "debounce"?
            if( !digitalRead(pin) ){
               fsmButtonState = STATE_BUTTON_DOWN;
            } else{
               fsmButtonState = STATE_BUTTON_UP;
            }
            contFalling = 0;
         }
         /* LEAVE */
         if( fsmButtonState != STATE_BUTTON_FALLING ){
            flagFalling = 0;
         }
      break;

      case STATE_BUTTON_RISING:      
         /* ENTRY */
         if( flagRising == 0 ){
            flagRising = 1;
         }    
         /* CHECK TRANSITION CONDITIONS */
         
         if( millis() - contRising >= 40 ){
            if( digitalRead(pin) ){
               fsmButtonState = STATE_BUTTON_UP;
            } else{
               fsmButtonState = STATE_BUTTON_DOWN;
            }
            contRising = 0;
         }
         
         /* LEAVE */
         if( fsmButtonState != STATE_BUTTON_RISING ){
            flagRising = 0;
         }
      break;

      default:
         //fsmButtonError();
      break;
   }
}